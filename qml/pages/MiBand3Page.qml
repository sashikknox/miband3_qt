import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQml.Models 2.2
import ru.sashikknox 1.0

Page {
    id: page
    allowedOrientations: Orientation.All
    property DeviceInfo device
//    property AuthService name: value
    property string lastError: device.lastError
    property bool authorized: device.authorized

    onDeviceChanged: {
        // 1 need discover all services
        // 2 get auth service and authenticate to MiBand3
//        device.authorize();
//        device.
    }

    Connections {
        target: device
        onDeviceScanDone: {
            console.debug("Device scan done");
            device.authorize()
        }
    }

    onAuthorizedChanged: {
        console.debug("Device authrized: " + String(authorized) )
    }

    ObjectModel {
        id: objects
        Column {
            id: column
//            anchors.left: list.left
//            anchors.right: list.right
            property int alertType: AlertNotification.Sms
            anchors.margins: Theme.paddingMedium
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingMedium

            Label {
                text: "Device is " + (authorized === true ? "authorized" : "not authorized")
            }

            TextArea {
                id: textInput
                anchors.left: parent.left
                anchors.right: parent.right
                height: 300

                focus: true
                font.family: "cursive"
            }

            ComboBox {
                id: alertTypeCB
                menu: ContextMenu {
                    MenuItem {
                        text: "SMS"
                        onClicked: {
                            column.alertType = AlertNotification.Sms
                        }
                    }
                    MenuItem {
                        text: "Email"
                        onClicked: {
                            column.alertType = AlertNotification.Email
                        }
                    }
                    MenuItem {
                        text: "Call"
                        onClicked: {
                            column.alertType = AlertNotification.Call
                        }
                    }
                    MenuItem {
                        text: "MissedCall"
                        onClicked: {
                            column.alertType = AlertNotification.MissedCall
                        }
                    }
                }
            }

            Button {
                id: sendCustomAlertBtn
                text: "Custom Alert"

                onClicked: {
                    // send notification
                    var text = (textInput.text.length == 0)?"Hello Sailor!":textInput.text
                    device.alert().sendCustomAlert(text,column.alertType);
                }
            }

            Button {
                id: sendAlertBtn
                text: "Alert"
                onClicked: {
                    // send notification
//                    var text = (textInput.text.length == 0)?"Hello Sailor!":textInput.text
                    device.alert().sendAlert(column.alertType);
                }
            }

            Label {
                text: "Device Info"
            }

        }
    }



    SilicaListView {
        id: list
        anchors.fill: parent
        model: objects
        header:PageHeader {
            title: device.name
        }

        VerticalScrollDecorator {}
    }
}
