import QtQuick 2.2
import QtQml.Models 2.2
import Sailfish.Silica 1.0
import ru.sashikknox 1.0

Page {
    id: page
    allowedOrientations: Orientation.All
    property ServiceInfo service

    ObjectModel {
        id: objectModel

        Column {
            id: mainColumn
            spacing: Theme.paddingSmall
            anchors.left: parent.left
            anchors.right: parent.right

            Repeater {
                anchors.leftMargin: Theme.horizontalPageMargin
                id: repeater
                model: service.charCount

                delegate: BackgroundItem {
                    property CharInfo charInfo: service.characteristic(index);
                    height: itemColumn.height
                    anchors.topMargin: Theme.paddingSmall
                    anchors.bottomMargin: Theme.paddingSmall

                    Column {
                        id: itemColumn
                        spacing: Theme.paddingSmall

                        Label {
                            id: lname
                            text: charInfo.cName === "" ? "Noname" : charInfo.cName
                            height: contentHeight
                        }

                        Label {
                            id: luuid
                            text: charInfo.cUuid
                            anchors.leftMargin: Theme.paddingLarge
                            color: Theme.secondaryColor
                            height: contentHeight
                        }

                        Label {
                            anchors.leftMargin: Theme.paddingLarge
                            text: "Descriptors"
                            color: Theme.secondaryColor
                            visible: charInfo.descriptorsCount > 0
                        }

                        Label {
                            anchors.leftMargin: Theme.paddingLarge
                            text: "Value " + charInfo.value
                            color: Theme.secondaryColor
                            visible: charInfo.descriptorsCount > 0
                        }

                        Label {
                            anchors.leftMargin: Theme.paddingLarge
                            text: "Handle " + charInfo.handle
                            color: Theme.secondaryColor
                            visible: charInfo.descriptorsCount > 0
                        }

                        Label {
                            anchors.leftMargin: Theme.paddingLarge
                            text: "Permission " + charInfo.permission
                            color: Theme.secondaryColor
                            visible: charInfo.descriptorsCount > 0
                        }

                        Repeater {
                            id: desc
                            anchors.leftMargin: Theme.paddingLarge
                            model: charInfo.descriptorsCount

                            delegate: Label {
                                font.pixelSize: Theme.fontSizeSmall * 0.8
                                text: "descriptor " + charInfo.desc(index).name + " {" + charInfo.desc(index).uuid + "}"
                                color: Theme.secondaryColor
                            }
                        }
                    }
                }
            }
        }
    }

    SilicaListView {
        id: list
        anchors.fill: parent

        header: PageHeader {
            title: service.serviceName
        }
        model: objectModel
        VerticalScrollDecorator {}
    }
}
