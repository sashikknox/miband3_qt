import QtQuick 2.2
import QtQml.Models 2.2
import QtBluetooth 5.2
import Sailfish.Silica 1.0
import ru.sashikknox 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All
    property DeviceInfo device
    property string lastError: device.lastError

    ObjectModel {
        id: deviceInfoModel
        Column {
            spacing: Theme.paddingMedium
            anchors.left: parent.left
            anchors.right: parent.right

            Label {
                anchors.leftMargin: Theme.horizontalPageMargin
                text: qsTr("MAC address: ") + device.address
                //anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                anchors.leftMargin: Theme.horizontalPageMargin
                id: errorLabel
                visible: false
                text: "no"
            }

            Connections {
                target: device
                onServicesUpdated: {
                    //repeater.model = device.servicesList
//                    console.log(device.serviceCount)
                }
            }

            Repeater {
                id:repeater
                model: device.serviceCount
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.topMargin: Theme.paddingMedium
                delegate: BackgroundItem {
                    height: serviceColumn.height
                    property ServiceInfo service: device.service(index)
                    property int serviceState: service.serviceState

                    onClicked:  {
                        service.discoveryDetails();

                        service.onServiceStateChanged.connect( function() {
                            if( serviceState === 3 )
                            {
                                pageStack.push(Qt.resolvedUrl("CharacteristicsPage.qml"), {service:service})
                            }
                        });
                    }

                    onServiceStateChanged: {
                        console.log ("State changed " + String(serviceState) )

//                        if( serviceState === 3 )
//                        {
//                            pageStack.push(Qt.resolvedUrl("CharacteristicsPage.qml"), {service:service})
//                        }
                    }

                    Column
                    {
                        id: serviceColumn
                        spacing: Theme.paddingMedium
                        Label {
                            id: sName
                            text: "name "+ service.serviceName
                        }

                        Label {
                            color: Theme.secondaryColor
                            id: sUuid
                            font.pixelSize: Theme.fontSizeTiny
                            text: "uuid " + service.serviceUuid
                        }

                        Label {
                            color: Theme.secondaryColor
                            id: sState
                            text: "state " + service.serviceState
                        }
                    }
                }
            }

        }
    }

    onLastErrorChanged: {
        errorLabel.text = String("\n") + lastError;
        errorLabel.visible = true;
    }

    SilicaListView {
        id: listView
        anchors.fill: parent

        PullDownMenu {
            id: pdm
            MenuItem {
                text: qsTr("Connect to MiBand3")
                onClicked: {
                   device.authorize();
                }
            }
        }

        header: PageHeader {
            title: device.name
        }

        model: deviceInfoModel
        VerticalScrollDecorator {}
    }
}
