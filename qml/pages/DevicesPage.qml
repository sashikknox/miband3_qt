import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.sashikknox 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaListView {
        id: devicesList
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Start search")
                onClicked: {
                    deviceManager.startSearch()
                }
            }
            MenuItem {
                text: qsTr("Stop search")
                onClicked: {
                    deviceManager.stopSearch()
                }
            }
        }

        model: DeviceSearchModel {
            id: deviceManager
        }

        delegate: ListItem {
            anchors.margins: Theme.paddingMedium
            height: deviceInfo.contentHeight

            //anchors.fill: parent

            Component {
                id: deviceInfoPage
                DeviceInfoPage {
                }
            }

            Component {
                id: miPage
                MiBand3Page {
                }
            }

            onClicked: {
                deviceManager.stopSearch();
                pageStack.pushAttached(
                            miPage,
                            {
                                device: deviceManager.selectDevice(index)
                            } );
                pageStack.navigateForward();
            }

            Column {
                id: deviceInfo
                //anchors.margins: Theme.paddingMedium
                spacing: Theme.paddingMedium
                Label {
                    id: deviceName
                    text: role_name
                }
                Label {
                    id: macAddress
                    text: role_mac_address
                    font.pixelSize: Theme.fontSizeTiny
                }
            }
        }
    }
}
