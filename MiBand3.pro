# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = MiBand3
VERSION = 0.0.1

DEFINES += APP_VERSION=\\\"$$VERSION\\\"

CONFIG += sailfishapp
#CONFIG += link_pkgconfig
QT += bluetooth
PKGCONFIG += openssl

SOURCES += \
    src/AlertNotificationService.cpp \
    src/main.cpp \
    src/MiBandConnector.cpp \
    src/DeviceSearchModel.cpp

DISTFILES += qml/MiBand3.qml \
    qml/cover/CoverPage.qml \
    rpm/MiBand3.changes.in \
    rpm/MiBand3.changes.run.in \
    rpm/MiBand3.spec \
    rpm/MiBand3.yaml \
    translations/*.ts \
    MiBand3.desktop \
    qml/pages/DevicesPage.qml \
    qml/pages/DeviceInfoPage.qml \
    qml/pages/CharacteristicsPage.qml \
    qml/pages/MiBand3Page.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/MiBand3-de.ts

HEADERS += \
    src/AlertNotificationService.h \
    src/MiBandConnector.h \
    src/DeviceSearchModel.h
