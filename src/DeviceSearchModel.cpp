#include "DeviceSearchModel.h"
#include "DeviceSearchModel.h"
#include <QDebug>
#include <QBluetoothAddress>
#include <openssl/aes.h>
#include <openssl/rand.h>

static QMap<QString,QString> mi_services = {
    {"{0000fee0-0000-1000-8000-00805f9b34fb}","MiBand1"},
    {"{0000fee1-0000-1000-8000-00805f9b34fb}","MiBand2 auth"},
    {"{00001530-0000-3512-2118-0009af100700}","DFU_Firmware"},
};


static QMap<QString,QString> mi_characterisircs ={
    {"{00000002-0000-3512-2118-0009af100700}","HZ"},
    {"{00000001-0000-3512-2118-0009af100700}","SENSOR"},
    {"{00000009-0000-3512-2118-0009af100700}","AUTH"},
    {"{00002a37-0000-1000-8000-00805f9b34fb}","HEART_RATE_MEASURE"},
    {"{00002a39-0000-1000-8000-00805f9b34fb}","HEART_RATE_CONTROL"},
    {"{00002a06-0000-1000-8000-00805f9b34fb}","ALERT"},
    {"{00002a46-0000-1000-8000-00805f9b34fb}","CUSTOM_ALERT"},
    {"{00000006-0000-3512-2118-0009af100700}","BATTERY"},
    {"{00000007-0000-3512-2118-0009af100700}","STEPS"},
    {"{00000003-0000-3512-2118-0009af100700}","CONFIGURATION"},
    {"{00000010-0000-3512-2118-0009af100700}","DEVICEEVENT"},
    {"{00000008-0000-3512-2118-0009af100700}","USER_SETTINGS"},
    {"{00001531-0000-3512-2118-0009af100700}","DFU_FIRMWARE"},
    {"{00001532-0000-3512-2118-0009af100700}","DFU_FIRMWARE_WRITE"},
    {"{0000ff09-0000-1000-8000-00805f9b34fb}","LE_PARAMS"},
    {"{00002a28-0000-1000-8000-00805f9b34fb}","REVISION"},
    {"{00002a25-0000-1000-8000-00805f9b34fb}","SERIAL"},
    {"{00002a27-0000-1000-8000-00805f9b34fb}","HRDW_REVISION"},
    {"{00002a2b-0000-1000-8000-00805f9b34fb}","CURRENT_TIME"},
    {"{00002a80-0000-1000-8000-00805f9b34fb}","AGE"},
    {"{0000ff0f-0000-1000-8000-00805f9b34fb}","maybe its PAIR"} // not confirmed
};

//NOTIFICATION_DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb"
//# Device Firmware Update
//SERVICE_DFU_FIRMWARE = ""

CharacteristicInfo::CharacteristicInfo(QObject *parent)
    : QObject(parent)
{

}

CharacteristicInfo::CharacteristicInfo(const QLowEnergyCharacteristic &other, QObject *parent)
    : QObject(parent)
    , m_char(other)
{
	if( m_char.name().isEmpty())
	{
		QMap<QString,QString>::iterator
		        it = mi_characterisircs.find(m_char.uuid().toString());
		if( it != mi_characterisircs.end() )
		{
			m_name = it.value();
		}
	}
	for( int i = 0; i < other.descriptors().size(); i++ )
	{
		m_descriptor.append( new DescriptorInfo(other.descriptors().at(i)) );
	}
//	other.properties();
}

QString CharacteristicInfo::getUuid() const
{
	if(!m_char.isValid())
		return QString("Not valid");

	const QBluetoothUuid uuid = m_char.uuid();
	bool success = false;
	quint16 result16 = uuid.toUInt16(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result16, 16);

	quint32 result32 = uuid.toUInt32(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result32, 16);

	return uuid.toString().remove(QLatin1Char('{')).remove(QLatin1Char('}'));
}

QString CharacteristicInfo::getName() const
{
	if(!m_char.isValid())
		return QString("Not valid");
	if( !m_name.isEmpty())
		return m_name;
	return m_char.name();
}

int CharacteristicInfo::getDC() const
{
	return m_descriptor.size();
}

QString CharacteristicInfo::getValue() const
{
	// Show raw string first and hex value below
	QByteArray a = m_char.value();
	QString result;
	if (a.isEmpty()) {
		result = QStringLiteral("<none>");
		return result;
	}

	result = a;
	result += QLatin1Char('\n');
	result += a.toHex();

	return result;
}

QString CharacteristicInfo::getHandle() const
{
	return QStringLiteral("0x") + QString::number(m_char.handle(), 16);
}

QString CharacteristicInfo::getPermission() const
{
	QString properties = "( ";
	int permission = m_char.properties();
	if (permission & QLowEnergyCharacteristic::Read)
		properties += QStringLiteral(" Read");
	if (permission & QLowEnergyCharacteristic::Write)
		properties += QStringLiteral(" Write");
	if (permission & QLowEnergyCharacteristic::Notify)
		properties += QStringLiteral(" Notify");
	if (permission & QLowEnergyCharacteristic::Indicate)
		properties += QStringLiteral(" Indicate");
	if (permission & QLowEnergyCharacteristic::ExtendedProperty)
		properties += QStringLiteral(" ExtendedProperty");
	if (permission & QLowEnergyCharacteristic::Broadcasting)
		properties += QStringLiteral(" Broadcast");
	if (permission & QLowEnergyCharacteristic::WriteNoResponse)
		properties += QStringLiteral(" WriteNoResp");
	if (permission & QLowEnergyCharacteristic::WriteSigned)
		properties += QStringLiteral(" WriteSigned");
	properties += " )";
	return properties;
}

DescriptorInfo *CharacteristicInfo::desc(int i) const
{
	if( i < 0 || i >= m_descriptor.size() )
		return Q_NULLPTR;
	return m_descriptor[i];
}

ServiceInfo::ServiceInfo(QObject *parent)
    : QObject(parent)
    , m_service(Q_NULLPTR)
{
}

ServiceInfo::ServiceInfo(QLowEnergyService *service, QObject *parent)
    : QObject(parent)
    , m_service(service)
{
	m_service->setParent(this);
	QMap<QString,QString>::iterator
	        it = mi_services.find(service->serviceUuid().toString());
	if( it != mi_services.end() )
	{
		m_name = it.value();
	}
	m_state = service->state();

	connect(service, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
	        this, SLOT(serviceDetailsDiscovered(QLowEnergyService::ServiceState)));
}

QLowEnergyService *ServiceInfo::service() const
{
	return m_service;
}

QString ServiceInfo::getName() const
{
	if (!m_service)
		return QString();
	if(!m_name.isEmpty())
		return m_name;

	return m_service->serviceName();
}

QString ServiceInfo::getType() const
{
	if (!m_service)
		return QString();

	QString result;
	if (m_service->type() & QLowEnergyService::PrimaryService)
		result += QStringLiteral("primary");
	else
		result += QStringLiteral("secondary");

	if (m_service->type() & QLowEnergyService::IncludedService)
		result += QStringLiteral(" included");

	result.prepend('<').append('>');

	return result;
}

int ServiceInfo::serviceState() const
{
	return service()->state();
}

int ServiceInfo::charCount() const
{
	return m_characterisitcs.size();
}

void ServiceInfo::discoveryDetails()
{
	qDebug() << "Service discovery deatils requested";
	service()->discoverDetails();
}

CharacteristicInfo* ServiceInfo::characteristic(int index)
{
	return m_characterisitcs[index];
}

void ServiceInfo::serviceDetailsDiscovered(QLowEnergyService::ServiceState state)
{
	qDebug() << "Service state changed" << state;
	m_state = state;
	if( state == QLowEnergyService::ServiceDiscovered )
	{
		for(int i = 0; i <  service()->characteristics().size(); i++ )
		{
			m_characterisitcs.append( new CharacteristicInfo(m_service->characteristics().at(i), this) );
		}
		emit charCountChanged();
	}
	emit serviceStateChanged();
}

void ServiceInfo::characteristicChanged(const QLowEnergyCharacteristic &info, const QByteArray &value)
{

}

void ServiceInfo::characteristicRead(const QLowEnergyCharacteristic &info, const QByteArray &value)
{

}

void ServiceInfo::characteristicWritten(const QLowEnergyCharacteristic &info, const QByteArray &value)
{

}

void ServiceInfo::descriptorRead(const QLowEnergyDescriptor &info, const QByteArray &value)
{
	qDebug() << info.uuid();
}

void ServiceInfo::descriptorWritten(const QLowEnergyDescriptor &info, const QByteArray &value)
{
	qDebug() << info.uuid();
}

void ServiceInfo::error(QLowEnergyService::ServiceError error)
{
	qCritical() << error;
}

QString ServiceInfo::getUuid() const
{
	if (!m_service)
		return QString();

	const QBluetoothUuid uuid = m_service->serviceUuid();
	bool success = false;
	quint16 result16 = uuid.toUInt16(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result16, 16);

	quint32 result32 = uuid.toUInt32(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result32, 16);

	return uuid.toString().remove(QLatin1Char('{')).remove(QLatin1Char('}'));
}

DeviceSearchModel::DeviceSearchModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_current(Q_NULLPTR)
{
	m_discoveryAgent = new QBluetoothDeviceDiscoveryAgent();
	connect(m_discoveryAgent, SIGNAL(deviceDiscovered(const QBluetoothDeviceInfo&)),
	        SLOT(slot_addDevice(const QBluetoothDeviceInfo&)));
	connect(m_discoveryAgent, SIGNAL(error(QBluetoothDeviceDiscoveryAgent::Error)),
	        SLOT(slot_deviceScanError(QBluetoothDeviceDiscoveryAgent::Error)));
	connect(m_discoveryAgent, SIGNAL(finished()), SLOT(slot_deviceScanFinished()));
	m_discoveryAgent->start();
}

QVariant DeviceSearchModel::data(const QModelIndex &index, int role) const
{
	int i = index.row();
	if( m_devices.size() <= i || i < 0 )
		return QVariant("");
	if( role == MacAddres )
	{
		return m_devices[i].address().toString();
	}
	else if ( role == DeviceName )
		return m_devices[i].name();
	return QVariant("");
}

int DeviceSearchModel::rowCount(const QModelIndex &parent) const
{
	return m_devices.size();
}

DeviceInfo *DeviceSearchModel::selectDevice(int index)
{
	if(index < 0 || index >= m_devices.size() )
		return Q_NULLPTR;

	if(m_current) {
		delete m_current;
	}
	m_current = new DeviceInfo( m_devices[index], this);
	return m_current;
}

void DeviceSearchModel::startSearch()
{
	//m_discoveryAgent->isActive()
	m_discoveryAgent->stop();
	beginResetModel();
	m_devices.clear();
	endResetModel();
	m_discoveryAgent->start();
}

void DeviceSearchModel::stopSearch()
{
	m_discoveryAgent->stop();
}

QHash<int, QByteArray> DeviceSearchModel::roleNames() const
{
	static const QHash<int, QByteArray> names = {
	{ MacAddres,          "role_mac_address"  },
	{ DeviceName,         "role_name"         },};
	return names;
}

void DeviceSearchModel::slot_addDevice(const QBluetoothDeviceInfo &info)
{
	int last_row = m_devices.isEmpty() ? 0 : m_devices.size() - 1;
	qDebug() << QStringLiteral("Found device [%0] %1 (%2)").arg(m_devices.size()).arg(info.name()).arg(info.address().toString());
	bool first = m_devices.isEmpty();
	if(first)
		beginResetModel();
	else
		beginInsertRows( QModelIndex(), m_devices.size() - 1, m_devices.size());
	m_devices.push_back(info);
	if(first)
		endResetModel();
	else
		endInsertRows();
}

void DeviceSearchModel::slot_deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
	qCritical() << error;
}

void DeviceSearchModel::slot_deviceScanFinished()
{
//	beginResetModel();
//	endResetModel();
	qDebug() << "Device scan finished";
}

DeviceInfo::DeviceInfo(QObject *parent)
    : QObject(parent)
    , m_controller(Q_NULLPTR)
    , m_authService(Q_NULLPTR)
    , m_alertService(Q_NULLPTR)
    , m_authorized(false)
{
}

DeviceInfo::DeviceInfo(const QBluetoothDeviceInfo &info, QObject *parent)
    : QObject(parent)
    , m_controller(Q_NULLPTR)
    , m_authService(Q_NULLPTR)
    , m_alertService(Q_NULLPTR)
    , m_authorized(false)
{
	m_info = info;
	init();
}

DeviceInfo::~DeviceInfo()
{
	if(m_controller)
	{
		m_controller->disconnectFromDevice();
		delete m_controller;
		m_controller = Q_NULLPTR;
	}
}

void DeviceInfo::operator =(const DeviceInfo &other)
{
	m_info = other.m_info;
	init();
}

QString DeviceInfo::address() const
{
	return m_info.address().toString();
}

QString DeviceInfo::getName() const
{
	return m_info.name();
}

QString DeviceInfo::getLastError() const
{
	return m_lastError;
}

QVariant DeviceInfo::getServices()
{
	QStringList list;
	list << "Heelo" << "World" << "Many" << "Times";
//	return QVariant::fromValue(list);
	return QVariant::fromValue(m_services);
}

ServiceInfo *DeviceInfo::service(int index) const
{
	return m_services.at(index);
}

bool DeviceInfo::authorize()
{
	if(!m_controller) {
		qCritical() << "Controller not craterd";
		return false;
	}
	qDebug() << "Begin authorization";
	/*
	And here are the Authentication steps:
	1. Setting on auth notifications (to get a response) by sending 2
	   bytes request \x01\x00 to the Des. */
	//QLowEnergyService::writeDescriptor(const QLowEnergyDescriptor &descriptor, const QByteArray &newValue)
	QBluetoothUuid miband_2_service( QStringLiteral("{0000fee1-0000-1000-8000-00805f9b34fb}") );
	QLowEnergyService *service = m_controller->createServiceObject(miband_2_service);
	if (!service) {
		qWarning() << "Cannot create service for uuid" << miband_2_service;
		return false;
	}
	// TODO here we need get descriptors first, then authorize
	if(m_authService)
	{
		delete m_authService;
		m_authService = Q_NULLPTR;
	}
	m_authService = new AuthService(service,this);
	connect(m_authService, SIGNAL(authStateChanged()), SLOT(authStateChanged()));

	return true;
}

AlertNotificationService *DeviceInfo::alert() const
{
	return m_alertService;
}

void DeviceInfo::authStateChanged()
{
	if( m_authService->authState() == AuthService::Authorised )
	{
		QString uuid = QStringLiteral("{0000%1-0000-1000-8000-00805f9b34fb}").arg("1811");
		QLowEnergyService *service =  m_controller->createServiceObject(QBluetoothUuid(uuid));
		if (!service) {
			qWarning() << "Cannot create service for uuid" << uuid;
			return;
		}
		setAuthorized(true);
		// TODO here we need get descriptors first, then authorize
		if(m_alertService)
		{
			delete m_alertService;
			m_alertService = Q_NULLPTR;
		}
		m_alertService = new AlertNotificationService(service,this);
		m_alertService->setKey( m_authService->getKey() );
	}
}

int DeviceInfo::serviceCount() const
{
	return m_services.size();
}

bool DeviceInfo::isAuthorized() const
{
	return m_authorized;
}

void DeviceInfo::deviceConnected()
{
	qDebug() << "Device connected";
	m_controller->discoverServices();
//	QList<QBluetoothUuid> s =  m_controller->services();
//	foreach(QBluetoothUuid service, s)
//	{
//		qDebug() << service;
//	}
}

void DeviceInfo::errorReceived(QLowEnergyController::Error error)
{
	m_lastError = m_controller->errorString() ;
	qCritical() << m_lastError.toUtf8().data();
	emit lastErrorChanged();
}

void DeviceInfo::deviceDisconnected()
{
	qDebug() << "Device disconnected";
}

void DeviceInfo::addLowEnergyService(QBluetoothUuid uuid)
{
	QLowEnergyService *service = m_controller->createServiceObject(uuid);
	if (!service) {
		qWarning() << "Cannot create service for uuid" << uuid;
		return;
	}
	ServiceInfo *serv = new ServiceInfo(service);

	if( uuid.toString().compare("{0000fee1-0000-1000-8000-00805f9b34fb}") == 0 )
	{
		qDebug() << serv->getName();
	}
	m_services.append(serv);
	qDebug() << "Service "<< uuid << service->serviceName();
	emit servicesUpdated();
}

void DeviceInfo::serviceScanDone()
{
	qDebug() << "Service scan done";
	emit deviceScanDone();
}

void DeviceInfo::init()
{
	if(m_controller)
	{
		m_controller->disconnectFromDevice();
		delete m_controller;
		m_controller = Q_NULLPTR;
	}

	if(!m_controller)
	{
		m_controller = new QLowEnergyController(m_info);
		connect(m_controller, SIGNAL(connected()),
		        this, SLOT(deviceConnected()));
		connect(m_controller, SIGNAL(error(QLowEnergyController::Error)),
		        this, SLOT(errorReceived(QLowEnergyController::Error)));
		connect(m_controller, SIGNAL(disconnected()),
		        this, SLOT(deviceDisconnected()));
		connect(m_controller, SIGNAL(serviceDiscovered(QBluetoothUuid)),
		        this, SLOT(addLowEnergyService(QBluetoothUuid)));
		connect(m_controller, SIGNAL(discoveryFinished()),
		        this, SLOT(serviceScanDone()));
	}

//	if (isRandomAddress())
	    m_controller->setRemoteAddressType(QLowEnergyController::RandomAddress);
//	else
//	    m_controller->setRemoteAddressType(QLowEnergyController::PublicAddress);
		m_controller->connectToDevice();
}

void DeviceInfo::setAuthorized(bool auth)
{
	m_authorized = auth;
	emit authorizedChanged();
}



DescriptorInfo::DescriptorInfo(QObject *parent)
    : QObject(parent)
{

}

DescriptorInfo::DescriptorInfo(const QLowEnergyDescriptor &other, QObject *parent)
    : QObject(parent)
    , m_desc(other)
{

}

QString DescriptorInfo::getName() const
{
	return m_desc.name();
}
//{00000003-f000-f70c-b7b2-02f700424500}
//{00002902-0000-1000-8000-00805f9b34fb}
//{00000003-f000-f70c-b7b2-02f700424500}
//{00002902-0000-1000-8000-00805f9b34fb}
QString DescriptorInfo::getUuid() const
{
	const QBluetoothUuid uuid = m_desc.uuid();
	bool success = false;
	quint16 result16 = uuid.toUInt16(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result16, 16);

	quint32 result32 = uuid.toUInt32(&success);
	if (success)
		return QStringLiteral("0x") + QString::number(result32, 16);

	return uuid.toString().remove(QLatin1Char('{')).remove(QLatin1Char('}'));

	return uuid.toString();
}


// AUTHORIZATION SERVICE
AuthService::AuthService(QObject *parent)
    : QObject(parent)
    , m_service(Q_NULLPTR)
    , _step(0)
    , m_state(NotAuthorised)
{
	_KEY = QByteArray("\x01\x23\x45\x67\x89\x01\x22\x23\x34\x45\x56\x67\x78\x89\x90\x02");
//	QBitArray key;
	_send_key_cmd = QByteArray("\x01\x08");
	_send_key_cmd.append(_KEY);
	_send_rnd_cmd = QByteArray("\x02\x08");
	_send_enc_key = QByteArray("\x03\x08");
}

AuthService::AuthService(QLowEnergyService *service, QObject *parent)
    : QObject(parent)
    , m_service(service)
    , _step(0)
    , m_state(NotAuthorised)
{
	_KEY = QByteArray("\x01\x23\x45\x67\x89\x01\x22\x23\x34\x45\x56\x67\x78\x89\x90\x02");
	_send_key_cmd = QByteArray("\x01\x08");
	_send_key_cmd.append(_KEY);
	_send_rnd_cmd = QByteArray("\x02\x08");
	_send_enc_key = QByteArray("\x03\x08");

	connect(m_service, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
	        this, SLOT(stateChanged(QLowEnergyService::ServiceState)));
	connect(m_service, SIGNAL(characteristicChanged(const QLowEnergyCharacteristic&,const QByteArray&)),
	        this, SLOT(characteristicChanged(const QLowEnergyCharacteristic&,const QByteArray&)) );
	connect(m_service, SIGNAL(characteristicRead(QLowEnergyCharacteristic,QByteArray)),
	        this, SLOT(characteristicRead(QLowEnergyCharacteristic,QByteArray)));
	connect(m_service, SIGNAL(characteristicWritten(QLowEnergyCharacteristic,QByteArray)),
	        this,SLOT(characteristicWritten(QLowEnergyCharacteristic,QByteArray)));
	connect(m_service, SIGNAL(descriptorRead(const QLowEnergyDescriptor &,const QByteArray &)),
	        this, SLOT(descriptorRead(const QLowEnergyDescriptor &,const QByteArray &)));
	connect(m_service, SIGNAL(descriptorWritten(const QLowEnergyDescriptor &,const QByteArray &)),
	        this, SLOT(descriptorWritten(const QLowEnergyDescriptor &,const QByteArray &)));
	connect(m_service, SIGNAL(error(QLowEnergyService::ServiceError)),
	        this,SLOT(error(QLowEnergyService::ServiceError)));

	m_service->discoverDetails();
	return;
}

void AuthService::next_step()
{
	QString h;
	quint16 hi = auth_characterisitc.handle();

	qDebug() << "Hndle:" << hi;
	qDebug() << "Next step" << _step;
	switch(_step)
	{
	case 0:
		{
			QByteArray authFirstData("\x01\x00");
			++_step;
			qDebug() << "Send two bytes to Desc, for enabling notifications from AUTH char...";
			m_service->writeDescriptor( auth_descriptor, authFirstData );
		}
		break;
	case 1:
		{
			++_step;
			qDebug() << "Send encryption key with command to char...";
			QByteArray _send_data("\x01\x00",2);
			_send_data.append(_KEY);
			qDebug() << "KEY" << _send_data;
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
		}
		break;
	case 200:
		{
			++_step;
			qDebug() << "Send requesting random number to char...";
			QByteArray _send_data("\x02\x00");
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
		}
		break;
	}
}

void AuthService::setAuthState(int state)
{
	m_state = state;
	emit authStateChanged();
}

void AuthService::stateChanged(QLowEnergyService::ServiceState newState)
{
	if( newState == QLowEnergyService::ServiceDiscovered )
	{//
		QBluetoothUuid auth_char_uuid( QStringLiteral("{00000009-0000-3512-2118-0009af100700}") );
		auth_characterisitc = m_service->characteristic(auth_char_uuid);

		if( !auth_characterisitc.isValid() )
		{
			qCritical() << "Cant handle Auth cahracteristic" << auth_char_uuid;
			qDebug() << "Its has: ";
			for( int i = 0 ; i < m_service->characteristics().size(); i++ )
			{
				qDebug() << m_service->characteristics().at(i).uuid();
			}
			qDebug() << m_service->serviceName();
			return ;
		}
		QString f;
		{
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Unknown )
				f += "Unknown; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Broadcasting )
				f += "Broadcasting; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Read )
				f += "Read; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::WriteNoResponse )
				f += "WriteNoResponse; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Write )
				f += "Write; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Notify )
				f += "Notify; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::Indicate )
				f += "Indicate; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::WriteSigned )
				f += "WriteSigned; ";
			if( auth_characterisitc.properties() & QLowEnergyCharacteristic::ExtendedProperty)
				f += "ExtendedProperty; ";
		}
		qDebug() << f;

		QBluetoothUuid auth_desc_uuid( QStringLiteral("{00002902-0000-1000-8000-00805f9b34fb}") );
		auth_descriptor = auth_characterisitc.descriptor(auth_desc_uuid);
		if(!auth_descriptor.isValid()){
			qCritical() << "Cant handle Auth descriptor" << auth_desc_uuid;
			return;
		}


		qDebug() << "Enabling service notification status";
		QByteArray authFirstData("\x01\x00",2);
		qDebug() << "Send two bytes to Desc, for enabling notifications from AUTH char...";
		_step=1;
		m_service->writeDescriptor( auth_descriptor, authFirstData );
	}
}

//#include </Users/colombo/Programming/SailfishOS/mersdk/targets/SailfishOS-3.0.3.9-armv7hl/usr/include/openssl/aes.h>

QByteArray AuthService::encrypt(const QByteArray &chiper_key, const QByteArray &data)
{
	//AES/ECB/NoPadding
	AES_KEY enc_key;
	QByteArray out(data.size(), '\0');

	AES_set_encrypt_key((const unsigned char*)chiper_key.constData(), chiper_key.size() * 8, &enc_key);

	QByteArray padData = data;
	int padlen = ((data.size() + AES_BLOCK_SIZE) / AES_BLOCK_SIZE) * AES_BLOCK_SIZE - data.size();
	padData.append(QByteArray(padlen, (char)padlen));

	AES_ecb_encrypt((unsigned char*)data.constData(), (unsigned char*)out.data(),&enc_key,AES_ENCRYPT);
	return out;
}

QByteArray AuthService::decrypt(const QByteArray &chiper_key, const QByteArray &data)
{
	AES_KEY dec_key;
	QByteArray out(data.size(), '\0');
	AES_set_decrypt_key( (const unsigned char*)chiper_key.constData(), chiper_key.size() * 8, &dec_key);
	AES_ecb_encrypt((unsigned char*)data.constData(), (unsigned char*)out.data(),&dec_key,AES_DECRYPT);
	return out;
}

QByteArray AuthService::getRandomBytes(int bytes)
{
	QByteArray buff1(bytes, '\0');
	RAND_bytes((unsigned char*)buff1.data(), bytes);
	return buff1;
}

QByteArray AuthService::getKey() const
{
	return _KEY;
}

int AuthService::authState() const
{
	return m_state;
}

void AuthService::characteristicChanged(const QLowEnergyCharacteristic &info, const QByteArray &data)
{
	// get understand answer of MiBand 3
	QByteArray answer = data.left(3); // first 3 bytes - code of answer

	if( info == auth_characterisitc )
	{
		qDebug() << "Auth Characterisitc changed" << data;
		if(answer == QByteArray("\x10\x01\x01")){
//			self.device._req_rdn();
			qDebug() << "Requesting random number";
			QByteArray _send_data("\x02\x00",2);
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
		} else if ( answer  == QByteArray("\x10\x01\x04")) {
			qDebug() << "AUTH_STATES.KEY_SENDING_FAILED;";
		} else if ( answer  == QByteArray("\x10\x02\x01")) {
			qDebug() << "Send AES/ECB/NoPadding encrypted random number";
			QByteArray rnd_number = data.right(16);//last 16 bytes - random number
			/*Encrypting this random number with our 16 bytes key using
			 * the AES/ECB/NoPadding encryption algorithm (from Crypto.Cipher import AES)
			 * and send it back to the Char (\x03\x00 + encoded data)*/
			QByteArray enc_number = AuthService::encrypt(_KEY,rnd_number);
			QByteArray _send_data("\x03\x00",2);
			_send_data.append(enc_number);
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
		} else if ( answer  == QByteArray("\x10\x02\x04")) {
			qDebug() << "AUTH_STATES.REQUEST_RN_ERROR";
		} else if ( answer  == QByteArray("\x10\x03\x01")) {
			qDebug() << "AUTH_STATES.AUTH_OK";
			qDebug() << "Disable auth notifications";
			m_service->writeDescriptor( auth_descriptor, QByteArray("\x00\x00",2) );
			setAuthState(Authorised);
		} else if ( answer  == QByteArray("\x10\x03\x04")) {
//			self.device._send_key();
			qDebug() << "AUTH_STATES.ENCRIPTION_KEY_FAILED";
			qDebug() << "Send encryption key with command to char...";
			_step = 2;
			QByteArray _send_data("\x01\x00",2);
			_send_data.append(_KEY);
			qDebug() << "KEY" << _send_data;
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
		} else {
			qDebug() << "AUTH_STATES.AUTH_FAILED";
			setAuthState(AuthFailed);
		}
	}
	else {
		qDebug() << info.name() << data;
	}
}

void AuthService::characteristicRead(const QLowEnergyCharacteristic &info, const QByteArray &value)
{
	qDebug() << "Characteristic read ..";
	qDebug() << info.name() << value;
}

void AuthService::characteristicWritten(const QLowEnergyCharacteristic &info, const QByteArray &value)
{
	qDebug() <<  "Characteristic written ...";
	qDebug() << info.name() << value;
}

void AuthService::descriptorRead(const QLowEnergyDescriptor &info, const QByteArray &data)
{
	qDebug() << "Descriptor read ...";
	qDebug() << info.name() << data;
}

void AuthService::descriptorWritten(const QLowEnergyDescriptor &info, const QByteArray &data)
{
	qDebug() << QString("Descriptor %0 write:").arg(info.uuid().toString()) << data;
	if( info == auth_descriptor )
	{
		if ( data == QByteArray("\x01\x00",2) )
		{
			qDebug() << "Requesting random number";
			QByteArray _send_data("\x02\x00",2);
			m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
			qDebug() << "Enable AUTH characterisitc notifiactions";
			_step = 2 ;
			if(_step == 1)
			{
				// send encryption key to MiBand
				qDebug() << "Send encryption key with command to char...";
				_step = 2;
				QByteArray _send_data("\x01\x00",2);
				_send_data.append(_KEY);
				qDebug() << "KEY" << _send_data;
				m_service->writeCharacteristic( auth_characterisitc, _send_data, QLowEnergyService::WriteWithoutResponse );
			}
		}
	}
	//next_step();
}

void AuthService::error(QLowEnergyService::ServiceError error)
{
	qDebug() << error;
	qDebug() << auth_characterisitc.handle();
}
