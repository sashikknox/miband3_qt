#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "DeviceSearchModel.h"

int main(int argc, char *argv[])
{
	QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
	QScopedPointer<QQuickView> v(SailfishApp::createView());

	QCoreApplication::setApplicationVersion(APP_VERSION);
	QCoreApplication::setOrganizationDomain("harbour");
	QCoreApplication::setOrganizationName("sashikknox");


	qDebug() << QStringLiteral("App version: %0").arg(APP_VERSION);
	qmlRegisterType<DeviceSearchModel>("ru.sashikknox", 1, 0, "DeviceSearchModel");
	qmlRegisterType<DeviceInfo> ("ru.sashikknox", 1, 0, "DeviceInfo");
	qmlRegisterType<ServiceInfo>("ru.sashikknox", 1, 0, "ServiceInfo");
	qmlRegisterType<CharacteristicInfo>("ru.sashikknox", 1, 0, "CharInfo");
	qmlRegisterType<DescriptorInfo>("ru.sashikknox", 1, 0, "DescInfo");
	qmlRegisterType<AlertNotificationService>("ru.sashikknox", 1, 0, "AlertNotification");

//	qmlRegisterSingletonType<SettingsContainer>("ru.sashikknox", 1, 0, "Settings", SettingsContainer_singletontype_provider );
	v->setSource(SailfishApp::pathTo("qml/MiBand3.qml"));
	v->show();

//	qDebug() << "Run ecryption test";
//	QString plainText("Hello world");
//	QByteArray _KEY("\x01\x23\x45\x67\x89\x01\x22\x23\x34\x45\x56\x67\x78\x89\x90\x02");
//	qDebug() << "Plain text: " << plainText;
//	qDebug() << "Key" << _KEY;
//	QByteArray enc_text = AuthService::encrypt(_KEY,plainText.toUtf8());
//	qDebug() << "Encrypted:" << enc_text;
//	qDebug() << "Encrypted (from UTF8):" << QString::fromUtf8(enc_text);
//	QByteArray dec_text = AuthService::decrypt(_KEY,enc_text);
//	qDebug() << "Decrypted:" << dec_text;
//	qDebug() << "Decrypted (from UTF8):" << QString::fromUtf8(dec_text);

//	const char rawData[] = {
//	    '\x01','\x23','\x45','\x67',
//	    '\x89','\x01','\x22','\x23',
//	    '\x34','\x45','\x56','\x67',
//	    '\x78','\x89','\x90','\x02'
//	};
//	QByteArray fromS("\x01\x23\x45\x67\x89\x01\x22\x23\x34\x45\x56\x67\x78\x89\x90\x02");
//	QByteArray fromRd(rawData, sizeof(rawData) );

//	qDebug() << "fromS" << fromS;
//	qDebug() << "fromRW" << fromRd;
//	qDebug() << "fromS == fromRW:" << ((fromS == fromRd)?"true":"false");

//	fromS = QByteArray("\x01\x00",2);
//	fromRd = QByteArray();
//	fromRd.append(char(0x01));
//	fromRd.append(char(0x00));

//	qDebug() << "fromS" << fromS;
//	qDebug() << "fromRW" << fromRd;
//	qDebug() << "fromS == fromRW:" << ((fromS == fromRd)?"true":"false");

	return app->exec();;
}
