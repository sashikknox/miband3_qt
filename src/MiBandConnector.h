#ifndef MIBANDCONNECTOR_H
#define MIBANDCONNECTOR_H

#include <QObject>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QLowEnergyCharacteristic>
#include <QLowEnergyDescriptor>

/**
 * @brief The MiBandConnector class
 * simple class for control mi band 3 device
 */
class MiBandConnector : public QObject
{
	Q_OBJECT
public:
	explicit MiBandConnector(const QBluetoothDeviceInfo &info, QObject *parent = nullptr);

signals:

public slots:

protected:
};

#endif // MIBANDCONNECTOR_H
