#ifndef ALERTNOTIFICATIONSERVICE_H
#define ALERTNOTIFICATIONSERVICE_H

#include <QObject>
#include <QLowEnergyDescriptor>
#include <QLowEnergyCharacteristic>
#include <QLowEnergyService>

class AlertNotificationService : public QObject
{
	Q_OBJECT
public:
	enum AlertType {
		Email,
		Sms,
		Call,
		MissedCall
	};
	Q_ENUM(AlertType)
public:
	explicit AlertNotificationService(QObject *parent = Q_NULLPTR);
	explicit AlertNotificationService(QLowEnergyService *service, QObject *parent = Q_NULLPTR);

	void setKey(const QByteArray &key);

	Q_INVOKABLE void sendCustomAlert(QString message, AlertType type);
	Q_INVOKABLE void sendAlert(AlertType type);
signals:

private:
	void init();
	QString type2text(AlertType type) const;
	QByteArray type2key(AlertType type) const;
protected Q_SLOTS:
	void stateChanged(QLowEnergyService::ServiceState newState);
	void characteristicChanged(const QLowEnergyCharacteristic &info,const QByteArray&data);
	void characteristicRead(const QLowEnergyCharacteristic &info,
	                        const QByteArray &value);
	void characteristicWritten(const QLowEnergyCharacteristic &info,
	                           const QByteArray &value);
	void descriptorRead(const QLowEnergyDescriptor &info,const QByteArray &data);
	void descriptorWritten(const QLowEnergyDescriptor &info,const QByteArray &data);
	void error(QLowEnergyService::ServiceError error);

private:
	bool  m_discovered;
	QLowEnergyService *m_service;
	QByteArray _KEY;
	QLowEnergyCharacteristic alert_char;
	QLowEnergyCharacteristic custom_alert_char;

	QByteArray key_call;
	QByteArray key_email;
	QByteArray key_mcall; // missed call
	QByteArray key_sms;
};

#endif // ALERTNOTIFICATIONSERVICE_H
