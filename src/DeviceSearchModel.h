#ifndef DEVICESEARCHMODEL_H
#define DEVICESEARCHMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QBluetoothDeviceInfo>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QLowEnergyCharacteristic>
#include <QBluetoothUuid>

#include "AlertNotificationService.h"

class DescriptorInfo : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString name READ getName NOTIFY isChanged)
	Q_PROPERTY(QString uuid READ getUuid NOTIFY isChanged)
public:
	DescriptorInfo(QObject *parent = Q_NULLPTR);
	DescriptorInfo(const QLowEnergyDescriptor &other, QObject *parent = Q_NULLPTR);

	QString getUuid() const;
	QString getName() const;
Q_SIGNALS:
	void isChanged();
private:
	QLowEnergyDescriptor m_desc;
};

class CharacteristicInfo : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString cName READ getName NOTIFY isChanged)
	Q_PROPERTY(QString cUuid READ getUuid NOTIFY isChanged)
	Q_PROPERTY(int descriptorsCount READ getDC NOTIFY isDCChanged)
	Q_PROPERTY(QString value READ getValue NOTIFY isChanged)
	Q_PROPERTY(QString handle READ getHandle NOTIFY isChanged)
	Q_PROPERTY(QString permission READ getPermission NOTIFY isChanged)
public:
	CharacteristicInfo (QObject *parent = Q_NULLPTR);
	CharacteristicInfo (const QLowEnergyCharacteristic &other, QObject *parent = Q_NULLPTR);

	QString getUuid() const;
	QString getName() const;
	int getDC() const;
	QString getValue() const;
	QString getHandle() const;
	QString getPermission() const;

	Q_INVOKABLE DescriptorInfo *desc(int i) const;
Q_SIGNALS:
	void isChanged();
	void isDCChanged();
private:
	QLowEnergyCharacteristic m_char;
	QString m_name;
	QVector<DescriptorInfo*> m_descriptor;
};

class ServiceInfo : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString serviceName READ getName NOTIFY serviceChanged)
	Q_PROPERTY(QString serviceUuid READ getUuid NOTIFY serviceChanged)
	Q_PROPERTY(QString serviceType READ getType NOTIFY serviceChanged)
	Q_PROPERTY(int serviceState READ serviceState NOTIFY serviceStateChanged)
	Q_PROPERTY(int charCount READ charCount NOTIFY charCountChanged)

public:
	ServiceInfo(QObject *parent = Q_NULLPTR) ;
	ServiceInfo(QLowEnergyService *service, QObject *parent = Q_NULLPTR);
	QLowEnergyService *service() const ;
	QString getUuid() const;
	QString getName() const;
	QString getType() const;
	int serviceState() const;
	int charCount() const;

	Q_INVOKABLE void discoveryDetails();
	Q_INVOKABLE CharacteristicInfo* characteristic(int index) ;

protected Q_SLOTS:
	void serviceDetailsDiscovered(QLowEnergyService::ServiceState state);

	void characteristicChanged(const QLowEnergyCharacteristic &info,
	                           const QByteArray &value);
	void characteristicRead(const QLowEnergyCharacteristic &info,
	                        const QByteArray &value);
	void characteristicWritten(const QLowEnergyCharacteristic &info,
	                           const QByteArray &value);
	void descriptorRead(const QLowEnergyDescriptor &info,
	                    const QByteArray &value);
	void descriptorWritten(const QLowEnergyDescriptor &info,
	                       const QByteArray &value);
	void error(QLowEnergyService::ServiceError error);

Q_SIGNALS:
	void serviceChanged();
	void serviceStateChanged();
	void charCountChanged();

private:
	QVector<CharacteristicInfo*> m_characterisitcs;
	QLowEnergyService *m_service;
	QString m_name;
	int m_state;
};

class AuthService : public QObject {
	Q_OBJECT

	Q_PROPERTY(int authState READ authState NOTIFY authStateChanged)
public:
	enum AuthState {
		NotAuthorised,
		Authorised,
		AuthFailed,
	};
	Q_ENUM(AuthState)
public:
	AuthService(QObject *parent = Q_NULLPTR);
	AuthService(QLowEnergyService *service, QObject *parent = Q_NULLPTR);

	static QByteArray encrypt(const QByteArray &chiper_key, const QByteArray &data);
	static QByteArray decrypt(const QByteArray &chiper_key, const QByteArray &data);
	static QByteArray getRandomBytes(int bytes);
	QByteArray getKey() const;

	int authState() const;
protected:
	void next_step();
	void setAuthState(int state);
Q_SIGNALS:
	void authStateChanged();
protected Q_SLOTS:
	void stateChanged(QLowEnergyService::ServiceState newState);
	void characteristicChanged(const QLowEnergyCharacteristic &info,const QByteArray&data);
	void characteristicRead(const QLowEnergyCharacteristic &info,
	                        const QByteArray &value);
	void characteristicWritten(const QLowEnergyCharacteristic &info,
	                           const QByteArray &value);
	void descriptorRead(const QLowEnergyDescriptor &info,const QByteArray &data);
	void descriptorWritten(const QLowEnergyDescriptor &info,const QByteArray &data);
	void error(QLowEnergyService::ServiceError error);
private:
	QLowEnergyService *m_service;

	QByteArray _KEY;
	QByteArray _send_key_cmd;
	QByteArray _send_rnd_cmd;
	QByteArray _send_enc_key;
	int  m_state;

	QLowEnergyDescriptor auth_descriptor;
	QLowEnergyCharacteristic auth_characterisitc;
	int _step;
};

class DeviceInfo : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString address READ address NOTIFY addressChanged)
	Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
	Q_PROPERTY(QString lastError READ getLastError NOTIFY lastErrorChanged)
	Q_PROPERTY(QVariant servicesList READ getServices NOTIFY servicesUpdated)
	Q_PROPERTY(int serviceCount READ serviceCount NOTIFY servicesUpdated)
	Q_PROPERTY(bool authorized READ isAuthorized NOTIFY authorizedChanged)
public:
	DeviceInfo(QObject *parent = Q_NULLPTR);
	DeviceInfo(const QBluetoothDeviceInfo& info, QObject *parent = Q_NULLPTR);
	~DeviceInfo();

	void operator =(const DeviceInfo &other);
	QString address() const;
	QString getName() const;
	QString getLastError() const;

	QVariant getServices();
	int serviceCount() const;

	bool isAuthorized() const;

	Q_INVOKABLE ServiceInfo *service(int index) const;
	Q_INVOKABLE bool authorize();
	Q_INVOKABLE AlertNotificationService* alert() const;
protected Q_SLOTS:
	void deviceConnected();
	void errorReceived(QLowEnergyController::Error error);
	void deviceDisconnected();
	void addLowEnergyService(QBluetoothUuid uuid);
	void serviceScanDone();
	void authStateChanged();
private:
	void init();
	void setAuthorized(bool auth);

Q_SIGNALS:
	void lastErrorChanged();
	void addressChanged();
	void nameChanged();
	void servicesUpdated();
	void authorizedChanged();
	void deviceScanDone();
protected:
	QBluetoothDeviceInfo m_info;
	QString m_lastError;
	QLowEnergyController *m_controller;
	QVector<ServiceInfo*> m_services;
	bool m_authorized;

	AuthService *m_authService;
	AlertNotificationService *m_alertService;
};

class DeviceSearchModel : public QAbstractListModel
{
	Q_OBJECT
public:
	enum DataRole {
		MacAddres = Qt::UserRole,
		DeviceName,
	};

public:
	DeviceSearchModel(QObject *parent = Q_NULLPTR);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	virtual QHash<int,QByteArray> roleNames() const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	Q_INVOKABLE DeviceInfo *selectDevice(int index);

	Q_INVOKABLE void startSearch();
	Q_INVOKABLE void stopSearch();
protected Q_SLOTS:
	void slot_addDevice(const QBluetoothDeviceInfo& info);
	void slot_deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error);
	void slot_deviceScanFinished();
private:
	QBluetoothDeviceDiscoveryAgent *m_discoveryAgent;
	QVector<QBluetoothDeviceInfo> m_devices;

	DeviceInfo *m_current;
};

#endif // DEVICESEARCHMODEL_H
