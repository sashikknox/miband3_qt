#include "AlertNotificationService.h"
#include "DeviceSearchModel.h"

AlertNotificationService::AlertNotificationService(QObject *parent)
    : QObject(parent)
    , m_service(Q_NULLPTR)
    , m_discovered(false)
{
	init();
}

AlertNotificationService::AlertNotificationService(QLowEnergyService *service, QObject *parent)
    : QObject(parent)
    , m_service(service)
    , m_discovered(false)
{
	init();
	connect(m_service, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
	        this, SLOT(stateChanged(QLowEnergyService::ServiceState)));
	connect(m_service, SIGNAL(characteristicChanged(const QLowEnergyCharacteristic&,const QByteArray&)),
	        this, SLOT(characteristicChanged(const QLowEnergyCharacteristic&,const QByteArray&)) );
	connect(m_service, SIGNAL(characteristicRead(QLowEnergyCharacteristic,QByteArray)),
	        this, SLOT(characteristicRead(QLowEnergyCharacteristic,QByteArray)));
	connect(m_service, SIGNAL(characteristicWritten(QLowEnergyCharacteristic,QByteArray)),
	        this,SLOT(characteristicWritten(QLowEnergyCharacteristic,QByteArray)));
	connect(m_service, SIGNAL(descriptorRead(const QLowEnergyDescriptor &,const QByteArray &)),
	        this, SLOT(descriptorRead(const QLowEnergyDescriptor &,const QByteArray &)));
	connect(m_service, SIGNAL(descriptorWritten(const QLowEnergyDescriptor &,const QByteArray &)),
	        this, SLOT(descriptorWritten(const QLowEnergyDescriptor &,const QByteArray &)));
	connect(m_service, SIGNAL(error(QLowEnergyService::ServiceError)),
	        this,SLOT(error(QLowEnergyService::ServiceError)));

	m_service->discoverDetails();
}

void AlertNotificationService::setKey(const QByteArray &key)
{
	_KEY = key;
}

void AlertNotificationService::sendCustomAlert(QString message, AlertNotificationService::AlertType type)
{
	if( custom_alert_char.isValid() )
	{
		qDebug() << QStringLiteral("Send %0 custom alert notification").arg(type2text(type));
		QByteArray data;
		data.append(type2key(type));
		data.append( message.toUtf8() );
		m_service->writeCharacteristic( custom_alert_char, data, QLowEnergyService::WriteWithResponse);
	}
	else {
		qCritical() << "Custom alert characterisitic is not valid;";
	}
}

void AlertNotificationService::sendAlert(AlertNotificationService::AlertType type)
{
	if( alert_char.isValid() )
	{
		qDebug() << QStringLiteral("Send %0 alert notification").arg(type2text(type));
		m_service->writeCharacteristic( alert_char, type2key(type), QLowEnergyService::WriteWithResponse);
	}
	else {
		qCritical() << "Alert characterisitic is not valid;";
	}
}

void AlertNotificationService::init()
{
	key_email = QByteArray("\x01\x01",2);
	// 2 ?
	key_call  = QByteArray("\x03\x01",2);
	key_mcall = QByteArray("\x04\x01",2);
	key_sms   = QByteArray("\x05\x01",2);
}

QString AlertNotificationService::type2text(AlertNotificationService::AlertType type) const
{
	switch(type)
	{
	case Email:
		return QLatin1String("email");
	case Sms:
		return QLatin1String("sms");
	case Call:
		return QLatin1String("call");
	case MissedCall:
		return QLatin1String("missed call");
	//case Calendar:
	//	return QLatin1String("calendar");
	}
	return QLatin1String("unknown");
}

QByteArray AlertNotificationService::type2key(AlertNotificationService::AlertType type) const
{
	switch(type)
	{
	case Email:
		return key_email;
	case Sms:
		return key_sms;
	case Call:
		return key_call;
	case MissedCall:
		return key_mcall;
	default:
		return QByteArray("\x02\x01",2);
	}
}

void AlertNotificationService::stateChanged(QLowEnergyService::ServiceState newState)
{
	qDebug() << "Service state changed" << newState;
	if( newState == QLowEnergyService::ServiceDiscovered )
	{
		m_discovered = true;
		QBluetoothUuid uuid_alert(QLatin1String("{00002a06-0000-1000-8000-00805f9b34fb}"));
		QBluetoothUuid uuid_custom_alert(QLatin1String("{00002a46-0000-1000-8000-00805f9b34fb}"));

		custom_alert_char  = m_service->characteristic(uuid_custom_alert);
		alert_char = m_service->characteristic(uuid_alert);
	}
}

void AlertNotificationService::characteristicChanged(const QLowEnergyCharacteristic &info, const QByteArray &data)
{
	// get understand answer of MiBand 3
	QByteArray answer = data.left(2); // first 3 bytes - code of answer
	QByteArray snd = data.right(data.length() - 2);

	if( info == custom_alert_char )
	{
		if( answer == key_sms || answer == key_email || answer == key_call || answer == key_mcall )
			qDebug() << "Custom Alert Characterisitc changed" << QString::fromUtf8(snd);
		else
			qDebug() << "Custom Alert Characterisitc changed" << data;
	}
	else {
		qDebug() << info.name() << data;
	}
}

void AlertNotificationService::characteristicRead(const QLowEnergyCharacteristic &info, const QByteArray &value)
{
	qDebug() << "Characteristic read ..";
	qDebug() << info.name() << value;
}

void AlertNotificationService::characteristicWritten(const QLowEnergyCharacteristic &info, const QByteArray &value)
{
	qDebug() <<  "Characteristic written ...";
	qDebug() << info.name() << value;
}

void AlertNotificationService::descriptorRead(const QLowEnergyDescriptor &info, const QByteArray &data)
{
	qDebug() << "Descriptor read ...";
	qDebug() << info.name() << data;
}

void AlertNotificationService::descriptorWritten(const QLowEnergyDescriptor &info, const QByteArray &data)
{
	qDebug() << QString("Descriptor %0 write:").arg(info.uuid().toString()) << data;
}

void AlertNotificationService::error(QLowEnergyService::ServiceError error)
{
	qDebug() << error;
}
